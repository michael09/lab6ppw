from django.shortcuts import render
from .models import Status
from .forms import Form
from django.http import HttpResponseRedirect

# Create your views here.


def index(request):
    if request.method == 'POST':
        form = Form(request.POST)
        if form.is_valid():
            status = request.POST['status']
            Status.objects.create(status=status)
            return HttpResponseRedirect('lab6')
    else:
        form = Form()
        hasil = Status.objects.all()
    return render(request, 'lab_6/index.html', {'form': form, 'status': hasil})

def profile(request):
    return render(request, 'lab_6/profile.html')
