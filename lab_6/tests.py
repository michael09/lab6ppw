from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Status
from .forms import Form
from .views import profile
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import unittest
import ast
# Create your tests here.
class Lab6UnitTest(TestCase):
    def test_lab6_url_is_exist(self):
        response = Client().get('/lab6')
        self.assertEqual(response.status_code, 200)

    def test_lab6_landing_page_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_index_func(self):
        found = resolve('/lab6')
        self.assertEqual(found.func, index)

    def test_lab6_landing_page_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        Status.objects.create(status='senang')
        counting = Status.objects.all().count()
        self.assertEqual(counting, 1)

    def test_form_status_has_placeholder_and_css_classes_and_maximum_300_character(self):
        form = Form()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('id="id_status"', form.as_p())
        self.assertIn('maxlength="300"', form.as_p())

    def test_form_validation_max_300_character(self):
        form = Form(data={'status': 'x'*310})
        self.assertFalse(form.is_valid())

    def test_lab6_post_success_and_render_the_result(self):
        test = 'hehehe'
        response_post = Client().post('/lab6', {'status': 'hehehe'})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/lab6')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_profile_url_is_exist(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code, 200)

    def test_profile_using_profile_func(self):
        found = resolve('/profile')
        self.assertEqual(found.func, profile)

    def test_profile_has_profile_and_css_classes(self):
        testnama = 'Nama:'
        testclass = "class='rounded isi'"
        response = Client().get('/profile')
        html_response = response.content.decode('utf8')
        self.assertIn(testnama, html_response)
        self.assertIn(testclass, html_response)


class Lab7FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.implicitly_wait(10)
        self.selenium.quit()

    def test_input(self):
        selenium = self.selenium
        selenium.get('http://lab6wh.herokuapp.com/')
        isi = selenium.find_element_by_id("id_status")
        submit = selenium.find_element_by_id('submit')
        isi.send_keys('coba-coba3')
        submit.send_keys(Keys.RETURN)
        self.assertIn('coba-coba3', selenium.page_source)

    def test_margin_status(self):
        selenium = self.selenium
        selenium.get('http://lab6wh.herokuapp.com/')
        status = selenium.find_element_by_class_name("status")
        margin = status.value_of_css_property('margin')
        self.assertEqual('20px', margin)

    def test_position_body(self):
        selenium = self.selenium
        selenium.get("http://lab6wh.herokuapp.com/")
        isi = selenium.find_element_by_tag_name('body')
        position = isi.value_of_css_property('position')
        self.assertEqual('relative', position)

    def test_css(self):
        selenium = self.selenium
        selenium.get("http://lab6wh.herokuapp.com/")
        urlCss = selenium.find_element_by_css_selector('link').get_attribute('href')
        self.assertIn('https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', urlCss)

    def test_font_size_card_bootstrap(self):
        selenium = self.selenium
        selenium.get("http://lab6wh.herokuapp.com/")
        card = selenium.find_element_by_class_name('btn')
        font = card.value_of_css_property('font-size')
        self.assertEqual('16px', font)