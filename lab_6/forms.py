from django import forms


class Form(forms.Form):
    status = forms.CharField(label='', widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=300)