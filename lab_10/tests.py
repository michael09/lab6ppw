from django.test import TestCase, Client
from django.urls import resolve
from .views import subscribe
from .models import Subscriber
# Create your tests here.


class Lab10UnitTest(TestCase):

    def test_lab10_url_is_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_lab10_using_right_function(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, subscribe)

    def test_models_can_create_new_subscriber(self):
        Subscriber.objects.create(email='hehe@gmail.com', name='hehe', password='123')
        count = Subscriber.objects.all().count()
        self.assertEqual(count, 1)

    def test_check_email_is_exist(self):
        response = Client().post('/subscribe/check', {'email': 'aabbccdd@gmail.com'})
        self.assertEqual(response.status_code, 200)

    def test_check_email_is_not_duplicated(self):
        response = Client().post('/subscribe/check', {'email': 'aabbccdd@gmail.com'})
        self.assertFalse(response.json()['is_email'])

    def test_check_email_is_duplicated(self):
        Subscriber.objects.create(email='hehe@gmail.com', name='hehe', password='123')
        response = Client().post('/subscribe/check', {'email': 'hehe@gmail.com'})
        self.assertTrue(response.json()['is_email'])

    def test_user_can_post(self):
        response = Client().post("/subscribe/", {'name': "a", "email": "abcd@gmail.com", "password": "123"})
        self.assertTrue(response.json()['status'])

    def test_user_cannot_post_duplicated_email(self):
        Client().post("/subscribe/", {'name': "a", "email": "abcd@gmail.com", "password": "123"})
        response = Client().post("/subscribe/", {'name': "a", "email": "abcd@gmail.com", "password": "123"})
        self.assertFalse(response.json()['status'])