from django.urls import path
from .views import subscribe, checkEmail, getSubscriber
urlpatterns = [
    path('', subscribe, name='subscribe'),
    path('check', checkEmail),
    path('getdata', getSubscriber)
]