$(document).ready(function () {
    $('#btnsub').prop('disabled', true);
    var flag = [false, false, false, false];
    $('#name').on('input', function () {
        var input = $(this);
        check(input, 0);
        checkButton();
    });

    var timer = null;
    $('#email').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(function () {
            var input = $("#email");
            check(input, 1);
        }, 1000);
        checkButton();
    });

    $('#password').on('input', function () {
        var input = $(this);
        check(input, 2);
        checkButton();
    });

    var check = function (input, arr) {
        if (arr === 1) {
            var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var is_el = reg.test(input.val());
            if (is_el) {
                flag[arr] = true;
                checkEmail(input.val());
                return
            } else {
                $(input).parent().removeClass('alert-validate2');
                flag[arr] = false;
                checkButton();
            }
        } else {
            var is_el = input.val();
        }
        if (is_el) {
            hideValidate(input);
            flag[arr] = true;
        } else {
            flag[arr] = false;
            showValidate(input)
        }
    };

    function showValidate(input) {
        input.parent().addClass('alert-validate');
    }

    function hideValidate(input) {
        input.parent().removeClass('alert-validate');
    }

    var checkEmail = function (email) {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "/subscribe/check",
            headers:{
                "X-CSRFToken": csrftoken
            },
            data: {email: email},
            success: function (response) {
                var inptEmail = $("#email");
                if (response.is_email) {
                    showValidate(inptEmail);
                    inptEmail.parent().addClass('alert-validate2');
                    flag[3] = false;
                    checkButton()
                } else {
                    hideValidate(inptEmail);
                    inptEmail.parent().removeClass('alert-validate2');
                    flag[3] = true;
                    checkButton()
                }
            },
            error: function (error) {
                alert("Error, cannot get data from server")
            }
        })
    };

    var checkButton = function () {
        var bttn = $('#btnsub');
        for (var x = 0; x < flag.length; x++) {
            if (flag[x] === false) {
                bttn.prop('disabled', true);
                return
            }
        }
        bttn.prop('disabled', false);
    };

    $(function () {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $('form').on('submit', function (e) {
          e.preventDefault();
          $.ajax({
            method: "POST",
            url: '/subscribe/',
              headers:{
                "X-CSRFToken": csrftoken
            },
            data: $('form').serialize(),
            success: function (status) {
                if (status.status) {
                    var title1 = "<span class='form-title-1'> Thank you for subscribing ! </span>";
                    var title2 = "<span class='form-title-2'> You'll get the latest news :)</span>";
                    var button = '<button' + ' class="bttnn"' + ' onClick="deleteSubs(\'' + status.email + '\')">' +'<i class="del"></i></button>';
                    var html = '<div' + ' id="' + status.email + '"' + '>' + button + status.nama + '<br>' + status.email +'</div>';
                    $('.contain').append(html)
                } else {
                    var title1 = "<span class='form-title-1'> Sorry something error :(</span>";
                    var title2 = "<span class='form-title-2'> Please contact me !</span>";
                }
                $(".form-title-1").replaceWith(title1);
                $(".form-title-2").replaceWith(title2);
                for (var i = 0; i < flag.length; i++) {
                    flag[i] = false
                }
                $("#btnsub").prop('disabled', true);
                $('#email').val("");
                 $('#password').val("");
                  $('#name').val("");
            },
            error: function(error) {
                alert("Error, cannot connect to server")
            }
          });
        });
      });
});