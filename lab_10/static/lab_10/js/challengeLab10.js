$(document).ready(function () {
    $.ajax({
        method: "GET",
        url: "/subscribe/getdata",
        success: function (hasil) {
            var data = hasil.subs;
            for (var x = 0; x < data.length; x++) {
                var nama = data[x]['name'];
                var email = data[x]['email'];
                var button = '<button' + ' class="bttnn"' + ' onClick="deleteSubs(\'' + email + '\')">' +'<i class="del"></i></button>';
                var html = '<div' + ' id="' + email + '"' + '>' + button + nama + '<br>' + email +'</div>';
                $('.contain').append(html)
            }
        },
        error: function (error) {
            alert("Server tidak merespons")
        }
    });
});
var deleteSubs = function (id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
        method: "POST",
        url: "/subscribe/getdata",
        headers:{
        "X-CSRFToken": csrftoken
    },
        data: {id: id},
        success: function (count) {
            $("div[id='" + id + "']").remove();
        },
        error: function (error) {
            alert("Error, cannot get data from server")
        }
    });
    }