from django.shortcuts import render, get_object_or_404
from .forms import SubscribeForm
from .models import Subscriber
from django.http import JsonResponse
from django.forms.models import model_to_dict
# Create your views here.


def subscribe(request):
    if request.method == "POST":
        form = SubscribeForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            status = True
            try:
                Subscriber.objects.create(name=name, email=email, password=password)
            except:
                status = False
            return JsonResponse({'status': status, 'nama': name, 'email': email})
    Form = SubscribeForm()
    return render(request, 'lab_10/subscribe.html', {'form': Form})


def checkEmail(request):
    if request.method == "POST":
        email = request.POST['email']
        bool = Subscriber.objects.filter(pk=email).exists()
        return JsonResponse({'is_email': bool})


def getSubscriber(request):
    if request.method == 'POST':
        email = request.POST['id']
        hasil = get_object_or_404(Subscriber, pk=email)
        hasil.delete()
        return JsonResponse({'id': email})
    subs = Subscriber.objects.all().values('name', 'email')
    listsubs = list(subs)
    return JsonResponse({'subs': listsubs})
